| Version | Status |
| ------ | ------ |
| **2019.8.15.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-azurecli/badges/2019.8.15.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-azurecli/commits/2019.8.15.0) |
| **2019.8.9.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-azurecli/badges/2019.8.9.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-azurecli/commits/2019.8.9.0) |

---

# docker-azurecli

Docker image with: Docker + Git + Kubectl + Python + Azure Cli